<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArisansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arisans', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('nama_arisan');
            $table->string('slug');
            $table->uuid('bendahara');
            $table->integer('jumlah_peserta');

            $table->integer('iuran');
            $table->unsignedTinyInteger('periode');
            $table->string('rekening_bendahara');
            $table->date('mulai');
            $table->foreign('bendahara')->references('id')->on('users')->onDelete('cascade');
            // $table->foreign('periode')->references('id')->on('periode')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arisans');
    }
}
