<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Periode extends Model
{
    protected $table = 'periode';

    public function arisan()
    {
        return $this->hasMany(Arisan::class, 'periode', 'id');
    }
}
