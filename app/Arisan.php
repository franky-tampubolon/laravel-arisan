<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Traits\Uuid;

class Arisan extends Model
{
    protected $guarded = [];

    use Uuid;

    public function user()
    {
        return $this->belongsTo(User::class, 'bendahara', 'id');
    }
    public function periode()
    {
        return $this->hasMany(Periode::class, 'id', 'periode');
    }
}
