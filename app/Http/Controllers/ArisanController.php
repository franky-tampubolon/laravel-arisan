<?php

namespace App\Http\Controllers;

use App\Arisan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Str;

class ArisanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('arisan.index');
    }

    public function data()
    {
        $arisan = Arisan::orderBy('created_at', 'DESC')->with('user', 'periode')->get();
        return json_encode($arisan);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('arisan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return dd($request->bendahara);
        $arisan = Arisan::create([
            'nama_arisan' => $request->nama_arisan,
            'slug' => Str::slug($request->nama_arisan),
            'bendahara' => $request->bendahara,
            'iuran' => $request->iuran,
            'mulai' => $request->mulai,
            'rekening_bendahara' => $request->rekening_bendahara,
            'periode' => $request->periode,
            'jumlah_peserta' => $request->jumlah_peserta,
        ]);

        return $arisan;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function join(Arisan $arisan)
    {
        return view('arisan.join', compact('arisan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arisan = Arisan::where('id', $id)->first();
        return view('arisan/edit', compact('arisan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function welcome()
    {
        $arisan = Arisan::orderBy('created_at', 'DESC')->with('user', 'periode')->get();
        return view('welcome', ['arisan' => $arisan]);
    }
}
