<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\User;

class UserLevel extends Model
{
    protected $guarded = [];
    protected $table = 'user_levels';
}
