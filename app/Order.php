<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class Order extends Model
{
	use Uuid;
    protected $guarded = [];
}
