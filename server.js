const io = require("socket.io")(3000);

io.on("connection", socket => {
    socket.on("user-join", name => {
        socket.broadcast.emit("new-user", name);
        console.log(name + " join");
    });
    socket.on("send-chat", pesan => {
        socket.broadcast.emit("pesan", pesan);
    });
});
