<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'ArisanController@welcome');
Route::get('/join/{arisan}', 'ArisanController@join');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/user-level', 'UserLevelController@store');
Route::get('/user/create', 'UserController@create');
Route::post('/user/create', 'UserController@store');
Route::get('/user', 'UserController@index');
Route::get('/user/edit/{id}', 'UserController@edit');
Route::post('/user/data', 'UserController@data');

Route::get('/arisan', 'ArisanController@index');
Route::get('/arisan/edit/{id}', 'ArisanController@edit');
Route::get('/arisan/create', 'ArisanController@create');
Route::post('/arisan/data', 'ArisanController@data');
Route::post('/arisan/store', 'ArisanController@store');
Route::view('/chat', 'arisan.chat');

// Routenya tinggal diganti POST
Route::get('payment', 'Api\PaymentController@store');
