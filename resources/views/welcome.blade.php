@extends('layouts.master')
@section('title-page','Arisan Online || Home')
@push('header-script')
<style type="text/css">
  body {
    padding-top: 56px;

  }

  .jumbotron {
    background-image: url('https://www.mapan.id/blog/wp-content/uploads/2019/07/Logo-Gelar-Arisan-MAPAN.png');
    min-height: 320px;
    background-position: center;
    back
  }

  .welcome {
    padding: 0 100px 0 100px;
  }
</style>
@endpush
@section('content')
<div class="welcome">
  <!-- Jumbotron Header -->
  <header class="jumbotron my-4">
  </header>

  <!-- Page Features -->
  <div class="row text-center">
    @foreach($arisan as $a)
    <div class="card mx-3" style="width: 18rem;">
      <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS3JatOslHoYMByJTVh6oG_LOMv07-dYH3UZg&usqp=CAU" class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title">{{$a->nama_arisan}}</h5>

        <a href="/join/{{$a->id}}" class="btn btn-primary">Join</a>
      </div>
    </div>



    @endforeach


    <div class="fixed-bottom">
      <button style="margin-bottom:100px;margin-right:800px;z-index:999" class="btn btn-danger" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
        Chat
      </button>

      <div class="collapse" id="collapseExample">
        <div class="bungkus" style="height: 300px;background-color:aqua;width:300px">
          <div id="chatlist">

          </div>

          <div id="chatbox" class="ml-4" style="position: absolute;bottom:0">
            <form action="">
              <input type="text" name="" id="input">
              <button class="btn btn-md btn-success" type="submit">Send</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- /.row -->
  </div>






  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
  @endsection