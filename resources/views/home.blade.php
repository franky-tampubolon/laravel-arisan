@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <sidebar-component></sidebar-component>
        <level-user-component></level-user-component>
    </div>
</div>
@endsection