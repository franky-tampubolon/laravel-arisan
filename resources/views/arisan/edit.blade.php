@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <sidebar-component></sidebar-component>
        <edit-arisan-component :user="{{ json_encode(Auth::user()->id)}}" :arisan="{{json_encode($arisan)}}"></edit-arisan-component>
    </div>
</div>
@endsection