@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <sidebar-component></sidebar-component>
        <create-arisan-component :user="{{ json_encode(Auth::user()->id)}}"></create-arisan-component>
    </div>
</div>
@endsection