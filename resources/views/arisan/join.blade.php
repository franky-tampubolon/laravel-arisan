@extends('layouts.master')
@section('title-page','Arisan Online || Join')
@push('header-script')
<style type="text/css">
    body {
        padding-top: 56px;
        background-color: #f5f5f5;
    }
</style>
@endpush
@section('content')
<div class="row mb-5 p-3 bg-white">
    <div class="col-md-3">
        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS3JatOslHoYMByJTVh6oG_LOMv07-dYH3UZg&usqp=CAU">
    </div>
    <div class="col-md-9">
        <div class="card" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">{{$arisan->nama_arisan}}</h5>
                <h6 class="card-subtitle mb-2 text-muted">Peserta : {{$arisan->jumlah_peserta}}</h6>
                <p class="card-text">Iuran : {{$arisan->iuran}}</p>
                <p class="card-text">Periode : {{$arisan->periode}}</p>
                <p class="card-text">Mulai : {{$arisan->mulai}}</p>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Join!!
                </button>
                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Pembayaran</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="/api/payment/store" method="post">
                                <div class="modal-body">
                                    <div class="form-check">
                                        <h4>Pilih Bank :</h4>
                                        <input class="form-check-input" type="radio" name="method" id="exampleRadios1" value="bca" checked>
                                        <label class="form-check-label" for="exampleRadios1">
                                            BCA
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="method" id="exampleRadios2" value="permata">
                                        <label class="form-check-label" for="exampleRadios2">
                                            Permata
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nominal</label>
                                        <input type="number" class="form-control" name="amount" id="exampleInputEmail1" aria-describedby="emailHelp">

                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                                    <button type="submit" class="btn btn-success">Bayar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- disini nampilin keterangan yang lain, masih rencana sih. -->
<div class="row bg-white p-3 mb-5">
    <div class="col">
        <h4>Keterangan Lainnya</h4>
    </div>
</div>
<!-- Disini nanti nampilin arisan yang lain -->
<div class="others">
    <h5>ini nampilin arisan yang lain</h5>
    <div class="row text-center">
        <?php for ($i = 1; $i < 5; $i++) : ?>
            <div class="col-lg-3 col-md-6 mb-4">
                <div class="card h-100">
                    <img class="card-img-top" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS3JatOslHoYMByJTVh6oG_LOMv07-dYH3UZg&usqp=CAU" alt="">
                    <div class="card-body">
                        <h4 class="card-title">Arisan Si Untung</h4>
                        <p class="card-text">Dapet duitnya, dapet berkahnya !!!</p>
                    </div>
                    <a href="" class="btn btn-primary">Join !</a>

                </div>
            </div>
        <?php endfor; ?>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
@endsection