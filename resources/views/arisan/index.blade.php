@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <sidebar-component></sidebar-component>
        <arisan-component></arisan-component>
    </div>
</div>
@endsection