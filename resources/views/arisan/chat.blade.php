@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <sidebar-component></sidebar-component>
        <div class="col-md-9">
            <div id="chatlist">

            </div>

            <div id="chatbox" class="ml-4" style="position: absolute;bottom:0">
                <form action="">
                    <input type="text" name="" id="input">
                    <button class="btn btn-md btn-success" type="submit">Send</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection