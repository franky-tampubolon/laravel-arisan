@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div>
            <h1>Terimakasih</h1>
            <br>
            <h3>{{$response->status_message}}</h3>
            <a href="/" class="btn btn-md btn-warning">Kembali</a>
        </div>
    </div>
</div>
@endsection