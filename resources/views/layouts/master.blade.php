@include('layouts.partials.header')

<body>

  @include('layouts.partials.navbar')

  <!-- Page Content -->
  <div class="container-fluid">

    @yield('content')

  </div>

 @include('layouts.partials.footer')
