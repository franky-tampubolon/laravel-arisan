const socket = io("http://127.0.0.1:3000");
const chatbox = document.getElementById("chatbox");
const input = document.getElementById("input");
const list = document.getElementById("chatlist");

const name = "Customer";
chatlist("anda bergabung");
socket.emit("user-join", name);

socket.on("pesan", pesan => {
    chatlist(pesan.name + " : " + pesan.message);
});

socket.on("new-user", name => {
    chatlist(name + " bergabung");
});
chatbox.addEventListener("submit", e => {
    e.preventDefault();
    let msg = input.value;
    socket.emit("send-chat", { message: msg, name: name });
    chatlist("Anda" + " : " + msg);
    input.value = "";
});

function chatlist(pesan) {
    let div = document.createElement("div");
    div.innerText = pesan;
    list.append(div);
}
